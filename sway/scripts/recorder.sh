#!/usr/bin/bash

default_sink=$(pactl info | grep 'Default Sink' | cut -d':' -f 2 | xargs)
default_source=$(pactl info | grep 'Default Source' | cut -d':' -f 2 | xargs)
sink_name="wf-recorder_combined_output_and_input"
filename=$(date +%F_%T.mkv)
pid=$(pgrep wf-recorder)
record_frame=$1

pactl load-module module-null-sink sink_name=$sink_name
pactl load-module module-loopback sink=$sink_name source=$default_sink
pactl load-module module-loopback sink=$sink_name source=$default_source

if [ -z $pid ]; then
	case $record_frame in
		"s") wf-recorder --audio=$sink_name.monitor --file=$HOME/Videos/Recordings/$filename -g "$(swaymsg -t get_outputs | jq -r '.[] | select(.active) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | slurp)";;
		"w") wf-recorder --audio=$sink_name.monitor --file=$HOME/Videos/Recordings/$filename -g "$(swaymsg -t get_tree | jq -r '.. | select(.pid? and .visible?) | .rect | "\(.x),\(.y) \(.width)x\(.height)"' | slurp)";;
		"a") wf-recorder --audio=$sink_name.monitor --file=$HOME/Videos/Recordings/$filename -g "$(slurp)";;
		*) wf-recorder --audio=$sink_name.monitor --file=$HOME/Videos/Recordings/$filename -o "$(swaymsg -t get_outputs | jq -r '.[] | select(.focused) | .name')";;
	esac
else
	killall -s SIGINT wf-recorder
	notify-send "Recording Stopped"
	pactl list short modules | grep "sink=$sink_name" | cut -f1 | xargs -L1 pactl unload-module
	pactl list short modules | grep "sink_name=$sink_name" | cut -f1 | xargs -L1 pactl unload-module
fi
